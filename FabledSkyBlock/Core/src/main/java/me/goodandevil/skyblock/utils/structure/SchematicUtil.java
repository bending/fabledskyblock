package me.goodandevil.skyblock.utils.structure;

import com.sk89q.worldedit.*;
import com.sk89q.worldedit.bukkit.BukkitUtil;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.schematic.SchematicFormat;
import me.goodandevil.skyblock.SkyBlock;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.primesoft.asyncworldedit.api.IAsyncWorldEdit;
import org.primesoft.asyncworldedit.api.worldedit.IAsyncEditSessionFactory;

import java.io.File;
import java.io.IOException;

public class SchematicUtil {

    public static Float[] pasteSchematic(File schematicFile, org.bukkit.Location location) {
        if (!Bukkit.getPluginManager().isPluginEnabled("WorldEdit"))
            throw new IllegalStateException("Tried to generate an island using a schematic file without WorldEdit installed!");

        Runnable pasteTask = () -> {
            try {
                final CuboidClipboard cc = SchematicFormat.MCEDIT.load(schematicFile);
                EditSession es = getEditSession(new BukkitWorld(location.getWorld()));
                cc.paste(es, BukkitUtil.toVector(location), false);
            } catch (MaxChangedBlocksException | IOException | com.sk89q.worldedit.data.DataException e) {
                e.printStackTrace();
            }
        };

        if (Bukkit.getPluginManager().isPluginEnabled("FastAsyncWorldEdit") || Bukkit.getPluginManager().isPluginEnabled("AsyncWorldEdit")) {
            Bukkit.getScheduler().runTaskAsynchronously(SkyBlock.getInstance(), pasteTask);
        } else {
            Bukkit.getScheduler().runTask(SkyBlock.getInstance(), pasteTask);
        }

        return new Float[]{location.getYaw(), location.getPitch()};
    }

    private static EditSession getEditSession(BukkitWorld world) {
        EditSessionFactory factory = WorldEdit.getInstance().getEditSessionFactory();
        Plugin plugin = Bukkit.getPluginManager().getPlugin("AsyncWorldEdit");
        if (plugin != null && plugin.isEnabled()) {
            IAsyncEditSessionFactory sessionFactory = (IAsyncEditSessionFactory) factory;
            return (EditSession) sessionFactory.getThreadSafeEditSession(world, Integer.MAX_VALUE, null, ((IAsyncWorldEdit) plugin).getPlayerManager().getUnknownPlayer());
        } else
            return factory.getEditSession(world, Integer.MAX_VALUE);
    }

}
